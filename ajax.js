    $(function(){
        $("#btn").click(function(){
            var city = $('#city').val();
            if(!city) {
                alert('输入不能为空');
            }

            $.post("getWeather.php", {city:city}, function(data){
                if(data.error_response){
                    alert(data.error_response);
                }else{
                    var weatherData = data.weatherinfo;
                    var todayInfo   = "<ul><li><h2>"+weatherData.city+"</h2></li>";
                        todayInfo  += "<li>星期："+weatherData.week+"</li>";
                        todayInfo  += "<li>温度："+weatherData.temp1+"</li>";
                        todayInfo  += "<li>天气："+weatherData.weather1+"</li>";
                        todayInfo  += "<li>风力："+weatherData.fl1+"</li>";
                        todayInfo  += "<li>体感："+weatherData.index+"</li>";
                        todayInfo  += "<li>时刻："+weatherData.date_y+"</li>";
                        todayInfo  += "</ul>";

                    var weekDay = getWeek();
                    var weekInfo  = "<table><tr>";
                    for (var i = 1; i <= 5; i++) {
                        weekInfo += "<td><ul>";
                        weekInfo += "<li class='weekT'>"+weekDay[i]+"</li>";
                        weekInfo += "<li>温度："+weatherData['temp' + i]+"</li>";
                        weekInfo += "<li>天气："+weatherData['weather' + i]+"</li>";
                        weekInfo += "<li class='img'><img src='http://m.weather.com.cn/img/b"+weatherData['img' + i]+".gif'</li>";
                        weekInfo += "</ul></td>";
                    }
                    weekInfo += "</tr></table>";

                    $("#todayInfo").empty().append(todayInfo);
                    $("#weekInfo").empty().append(weekInfo);

                }},"json");
        });

    });

    // 获取星期周
    function getWeek()
    {
        var oDate = new Date();
        var oDay = oDate.getDay();

        var days = ['星期天', '星期一', '星期二', '星期三', '星期四', '星期五', '星期六'];

        return days.slice(oDay, days.length).concat(days.slice(0, oDay));
    }

